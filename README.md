# Information / Информация

| Property     | Value                                              |
| ------------ | -------------------------------------------------- |
| ID           | `ext_7f33fcfa`                                     |
| Type         | Add-on                                             |
| License      | GPL-3.0                                            |
| Language     | Russian                                            |
| Requirements | XenForo 2.1                                        |
| Authors      | [Yu Dunaev](mailto:dun43v@gmail.com)               |

Расширение функции **Поделиться страницей** для [**XenForo**](https://xenforo.com).

## Install / Установка

1. [Загрузить](https://gitlab.com/marketplace-xenforo/xenforo-ext-share-page/tags) архив с последней версией расширения.
2. Распаковать содержимое архива в `/src/addons/MARKETPLACE/ext_7f33fcfa/`, сохраняя структуру директорий.
3. Зайти в **AdminCP**, далее *Add-ons*, и установить необходимое расширение.

## Update / Обновление

1. [Загрузить](https://gitlab.com/marketplace-xenforo/xenforo-ext-share-page/tags) архив с новой версией расширения.
2. Распаковать содержимое архива в `/src/addons/MARKETPLACE/ext_7f33fcfa/`, сохраняя структуру директорий, заменяя существующие файлы и папки.
3. Зайти в **AdminCP**, далее *Add-ons*, и обновить необходимое расширение.

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
